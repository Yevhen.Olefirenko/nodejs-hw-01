import express from 'express';
import morgan from 'morgan';
import fs from 'fs';

const PORT = 8080;

const app = express();

app.use(express.json());
app.use(morgan('combined'));

// saveFiles START

const extensions = ['log', 'txt', 'yaml', 'json', 'xml', 'js']

const checkExtensions = (filename) => {
  return extensions.includes(filename.substr((filename.lastIndexOf('.') + 1)));
}

app.post('/api/files', async (req, res) => {
  try {

    const dir = './api/files';
    if (!fs.existsSync(dir)){
      fs.mkdirSync(dir, { recursive: true });
    }
    if (!req.body.filename || !req.body.content) {
      return res.status(400).json({message: 'Please specify \'content\' parameter'});
    }
    if (!checkExtensions(req.body.filename)) {
      return res.status(400).json({message: 'Please specify \'content\' parameter'});
    }
    // if (typeof(req.body.content) !== 'string' || typeof(req.body.filename) !== 'string') {
    //   return res.status(400).json({message: 'Please specify \'content\' parameter'});
    // }
    if (fs.existsSync(`api/files/${req.body.filename}`)) {
      return res.status(400).json({message: 'Please specify \'content\' parameter'});
    }
    fs.writeFile(`api/files/${req.body.filename}`, req.body.content, function(err) {
      if (err) {
        return res.status(400).json({message: 'Please specify \'content\' parameter'});
      }
    });
    res.status(200).json({message: 'File created successfully'});
  } catch (e) {
    res.status(500).json({message: 'Server error'});
  }
});

// saveFiles END

// getFiles START

app.get('/api/files', (req, res) => {
  try {
    let files = fs.readdirSync('api/files');
    res.status(200).json({
      message: 'Success',
      files: files
    });
    if (!files) {
      return res.status(400).json({message: 'Client error'});
    }
  } catch (e) {
    res.status(500).json({message: 'Server error'});
  }
});

// getFiles END

// getFile START

app.get('/api/files/:filename', (req, res) => {
  try {
    const {filename} = req.params;
    if (!filename) {
      return res.status(400).json({message: `No file with ${filename} filename found`});
    }
    if (!fs.readdirSync('api/files/').includes(filename)) {
      return res.status(400).json({message: `No file with ${filename} filename found`});
    }
    fs.readFile(`api/files/${filename}`, 'utf8', async function(err, data) {
      if (err) {
        return res.status(400).json({message: `No file with ${filename} filename found`});
      }
      const uploadedDate = (await fs.promises.stat(`api/files/${filename}`)).mtime
      res.status(200).json({
        message: 'Success',
        filename: filename,
        content: data,
        extension: filename.split('.').reverse()[0],
        uploadedDate: uploadedDate
      });
    });
  } catch (e) {
    res.status(500).json({message: 'Server error'});
  }
});

// getFile END

// editFile START

app.put('/api/files/:filename', async (req, res) => {
  try {
    const {filename} = req.params;
    const content = req.body.content;
    if (!filename) {
      return res.status(400).json({message: `Please specify 'content' parameter`});
    }
    if (!content) {
      return res.status(400).json({message: `Please specify 'content' parameter`});
    }
    if (!fs.readdirSync('api/files/').includes(filename)) {
      return res.status(400).json({message: `Please specify 'content' parameter`});
    }
    await fs.promises.writeFile(`api/files/${filename}`, content);

    res.status(200).json({message: 'File edited successfully'});
  } catch (e) {
    res.status(500).json({message: 'Server error'});
  }
});

// editFile END

// deleteFile START

app.delete('/api/files/:filename', async (req, res) => {
  try {
    const {filename} = req.params;
    if (!filename) {
      return res.status(400).json({message: `No file with ${filename} filename found`});
    }
    if (!fs.readdirSync('api/files/').includes(filename)) {
      return res.status(400).json({message: `No file with ${filename} filename found`});
    }
    await fs.unlinkSync(`api/files/${filename}`);

    res.status(200).json({message: 'File deleted successfully'});
  } catch(e) {
    res.status(500).json({message: 'Server error'});
  }
});

// deleteFile END

app.listen(PORT, () => console.log(`SERVER STARTED ON PORT ${PORT}`));
